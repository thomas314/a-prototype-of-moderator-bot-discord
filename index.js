const Discord = require("discord.js");
const bot = new Discord.Client();
const config = require("./config.json");
const prefix = config.prefix;


// const ytdl = require("ytdl-core");
// var queue = New Map();


bot.on("ready", () => {
  console.log("I'm ready !");

  bot.user.setActivity("Bientot je suis randomisable");
});

bot.on("message", async (message) => {
  if (message.author.bot) return;
  if (message.content.indexOf(prefix) !== 0) return;

  const args = message.content.slice(prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();

  // const serverQueue = queue.get(message.guild.id);

  if (command === "hello") {
    message.reply(
      `Hello, we are on ${bot.user.tag} and I'm in ${bot.guilds.size} guilds`
    );
  }

  if (command === "ping") {
    const msg = await message.channel.send("Pinging...");
    msg.edit(`Pong ! More seriously, the latency is ${
      msg.createdTimestamp - message.createdTimestamp
    }ms.
        The API Latency is ${Math.round(bot.ping)}ms`);
  }

  if (command === "kick") {
    if (!message.member.hasPermission("ADMINISTRATOR"))
      return message.reply(
        "Désolé vous n'avez pas la permission possible à cette action"
      );
    let member = message.mentions.members.first();
    if (!member)
      return message.reply("Veuillez mentionner un User valide, plz");
    if (!member.kickable)
      return message.channel.send(
        "Désolé, je ne peux kick cette personne. Avez-vous les pleins droits ?"
      );

    let reason = args.slice(1).join(" ");
    if (!reason) reason = "Raison non spécifiée";

    await member
      .kick(reason)
      .catch((e) =>
        message.reply(`Désolé, je ne peux pas le kicker! Erreur: ${e} `)
      );
    message.reply(" :imposteurs: Cet user à été kick !");
  }

  if (command === "ban") {
    if (!message.member.hasPermission("ADMINISTRATOR"))
      return message.reply(
        "Désolé vous n'avez pas la permission possible à cette action"
      );
    let member = message.mentions.members.first();
    if (!member)
      return message.reply("Veuillez mentionner un User valide, plz");
    if (!member.bannable)
      return message.channel.send(
        "Désolé, je ne peux ban cette personne. Avez-vous les pleins droits ?"
      );
      
      let reason = args.slice(1).join(" ");
      if (!reason) reason = "Raison non spécifiée";
      
      await member
      .ban(reason)
      .catch((e) =>
      message.reply(`Désolé, je ne peux pas le bannir! Erreur: ${e} `)
      );
      message.reply("Cet utilisateur a été ban !");
    }
    
    if (message.content === "join") {
      // Only try to join the sender's voice channel if they are in one themselves
      if (message.member.voice.channel) {
        const connection = await message.member.voice.channel.join();
      } else {
        message.reply("You need to join a voice channel first!");
      }
    }
  });
  bot.login(config.token);
  
  
  
  
  
  
  // if(command === 'play'){
    //     // PLay URL
    //     if(args[0]) return message.reply("Veuillez entrer un lien valide :p");
    
    //     play(message, serverQueue);
    // }





// async function play(message, serverQueue) {
//   const args= message.content.split(" ");

//   const voiceChannel= message.member.voiceChannel;
//   if(!voiceChannel) return message.reply("Vous devez vous trouver dans un salon vocal !");
//   const permission= voiceChannel.permissionsFor(message.client.user);
//   if(!permission.has('CONNECT') || !permission.has('SPEAK')) {
//     return message.channel.send("J'ai besoin de la permission pour me connecter et parler sur votre salon vocal :)")
//   }

//   const songInfo = await ytdl.getInfo(args[1]);
//   const song = {
//     title: songInfo.title,
//     url: songInfo.video_url,
//   };

//   if(!serverQueue){
//     const queueConstruct= {
//       textChannel: message.channel,
//       voiceChannel: voiceChannel,
//       connection: null,
//       songs: [],
//       volume: 5,
//       playing: true,
//     };

//     queue.set(message.guild.id, queueConstruct);  if (command === "join") {
//       if (message.member.voice.channel) {
//         const connection = await message.member.voice.channel.join();
//       } else {
//         message.reply("You need to join a voice channel first!");
//       }￼
//     }
//     queueConstruct.songs.push(song);

//     try {
//       var connection = await voiceChannel.join();
//       queueConstruct.connection = connection;
//       playSong(message.guild, queueConstruct.songs[0]);
//     } catch(err) {
//         console.log(err);
//         queue.delete(message.guild.id)
//         return message.channel.send("Il y a une erreur pour jouer ceci.. Erreur : " + err );
//     }
//   } 
//   else {
//     serverQueue.songs.push(song);
//     return message.channel.send(`${song.title} à été ajouté à la queue.`);
//   }

// }

// function playSong() {
//   const serverQueue = queue.get(guil￼d.id);

//   if(!song) {
//     serverQueue.voiceChannel.leave();
//     queue.delete(guild.id);
//     return;
//   }

//   const dispatcher = serverQueue.connection.playStream(ytdl(song.url))
//   .on("end", () => {
//     serverQueue.songs.shift();
//     playSong(guild, serverQueue.songs[0]);
//   })
//   .on("error", error => {
//     console.log(error);
//   })
//   dispatcher.setVolumeLogarithmic(serverQueue.volume / 5);
// }



// client.music = require("discord.js-musicbot-addon");

// client.on("ready", () => {

//   console.log(`Connecté sur le ${client.user.tag}!`);
//   const channel=client.channels.get("mychannelid");
//     channel
//     .join()
//     .then((connection) => {
//       // Yay, it worked!
//       console.log(" Successfully connected.");
//     })
//     .catch((e) => {
//       // Oh no, it errored! Let's log it to console :)
//       console.error(e);
//     });
// });

// client.music.start(client, {
//   youtubeKey: "AIzaSyB0LIPC-0URMVd_8WUwbczbMpC_hCdkNhQ",
//   botPrefix: "$",
// });

// client.on("message", async (message) => {
//   // Voice only works in guilds, if the message does not come from a guild,
//   // we ignore it
//   if (!message.guild) return;

  // if (message.content === "join") {
  //   // Only try to join the sender's voice channel if they are in one themselves
  //   if (message.member.voice.channel) {
  //     const connection = await message.member.voice.channel.join();
  //   } else {
  //     message.reply("You need to join a voice channel first!");
  //   }
  // }
// });

// client.on("message", (msg) => {
//   if (msg.content === "ping") {
//     msg.reply("pong");
//   }
// });
